# Hugo utils

This repository contains helper scripts for using (Go) hugo.

## start-hugo.sh

Downloads and starts the hugo version specified in ``FORCE_HUGO_VERSION`` or ``hugo-version.txt``.
A valid version is for example ``0.62.1``.

## License

GPL-3.0-only
