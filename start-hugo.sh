#! /bin/bash
#
# hugo utils - helper scripts for using (Go) hugo
# Copyright (C) 2020 Jonas Lochmann
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
#
# This scripts uses the FORCE_HUGO_VERSION environment variable or the
# hugo-version.txt file of the current directory to download and launch
# the specified hugo version. All parameters are passed dirctly to hugo.
#
# This script downloads the offical binaries from GitHub.

# START OF CONFIGURATION
ARCH="Linux-64bit"
HUGO_DOWNLOAD_DIR="${XDG_CACHE_HOME:-$HOME/.cache}/hugo-utils/hugo-binaries"
# END OF CONFIGURATION

set -e

if [ "$FORCE_HUGO_VERSION" == "" ]; then
  if [ -f "./hugo-version.txt" ]; then
    FORCE_HUGO_VERSION="$(cat "./hugo-version.txt")"
  else
    echo "I don't know which hugo version you want to run"
    echo "Please set FORCE_HUGO_VERSION or create a hugo-version.txt"
    exit 1
  fi
fi

if [[ ! "$FORCE_HUGO_VERSION" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
  echo "The specified version number does not look like a hugo version number"
  exit 1
fi

HUGO_BINARY_PATH="$HUGO_DOWNLOAD_DIR/hugo-$FORCE_HUGO_VERSION-$ARCH"
LOCKFILE="$HUGO_DOWNLOAD_DIR/download-lock"

mkdir -p "$HUGO_DOWNLOAD_DIR"

if [ ! -d "$HUGO_DOWNLOAD_DIR" ]; then
  echo "Could not create the hugo download directory"
  exit 1
fi

if [ ! -f "$HUGO_BINARY_PATH" ]; then
  echo "The specified hugo version is not yet downloaded"

  echo "waiting for the lock"
  exec {LOCKFILE_FD}>"$LOCKFILE"
  flock "$LOCKFILE_FD"

  if [ -f "$HUGO_BINARY_PATH" ]; then
    echo "The hugo version was downloaded already"
  else
    DOWNLOAD_URL="https://github.com/gohugoio/hugo/releases/download/v${FORCE_HUGO_VERSION}/hugo_${FORCE_HUGO_VERSION}_${ARCH}.tar.gz"
    echo "downloading hugo $DOWNLOAD_URL"

    rm -f "$HUGO_BINARY_PATH.tmp"
    wget "$DOWNLOAD_URL" -qO - | tar -xzO hugo > "$HUGO_BINARY_PATH.tmp"
    chmod u+x "$HUGO_BINARY_PATH.tmp"
    mv "$HUGO_BINARY_PATH.tmp" "$HUGO_BINARY_PATH"
  fi

  flock -u "$LOCKFILE_FD"
fi

"$HUGO_BINARY_PATH" "$@"
